set nocompatible

filetype off
call plug#begin('~/.vim/plugged')
Plug 'altercation/vim-colors-solarized'
Plug 'scrooloose/nerdtree'
Plug 'scrooloose/syntastic'
Plug 'Lokaltog/vim-easymotion'
Plug 'gorodinskiy/vim-coloresque'
Plug 'SirVer/ultisnips'
Plug 'Ioannis-Kapoulas/vim-autoprefixer'
Plug 'ntpeters/vim-better-whitespace'
Plug 'jlanzarotta/bufexplorer'
call plug#end()

filetype plugin on
filetype plugin indent on

" enable mouse
set mousehide
set nomousefocus
set mousemodel=extend
set mouse=a

set number
syntax on
autocmd VimEnter * NERDTree
autocmd VimEnter * wincmd p
set guifont=Source\ Code\ Pro:h12 
colorscheme solarized

set directory=/tmp
set visualbell

au BufNewFile,BufRead *.less set filetype=css

function! NERDTreeQuit()
  redir => buffersoutput
  silent buffers
  redir END
"                     1BufNo  2Mods.     3File           4LineNo
  let pattern = '^\s*\(\d\+\)\(.....\) "\(.*\)"\s\+line \(\d\+\)$'
  let windowfound = 0

  for bline in split(buffersoutput, "\n")
    let m = matchlist(bline, pattern)

    if (len(m) > 0)
      if (m[2] =~ '..a..')
        let windowfound = 1
      endif
    endif
  endfor

  if (!windowfound)
    quitall
  endif
endfunction
autocmd WinEnter * call NERDTreeQuit()

imap <F2> <ESC>:BufExplorer<CR>
map <F2> :BufExplorer<CR>
map <F3> :NERDTree<CR>

set smartindent
set tabstop=4
set shiftwidth=4
set expandtab
let NERDTreeShowBookmarks=1

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

" syntastic
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:statline_syntastic = 0

let g:syntastic_javascript_checkers = ['jshint']

" easy motion
nmap <Leader>l <Plug>(easymotion-lineforward)
nmap <Leader>j <Plug>(easymotion-j)
nmap <Leader>k <Plug>(easymotion-k)
nmap <Leader>h <Plug>(easymotion-linebackward)
nmap <Leader>w <Plug>(easymotion-w)
nmap <Leader>W <Plug>(easymotion-W)
nmap <Leader>b <Plug>(easymotion-b)
nmap <Leader>B <Plug>(easymotion-B)
