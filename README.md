My VIM Configuration
====================

To install, run the following:

```bash
$ git clone http://bitbucket.org/synic/binksvim ~/.vim && \
    cd ~/.vim && \
    ./install.sh
```
