#!/bin/bash

git submodule init
git submodule update

CWD=`pwd`
cd ~
ln -sf .vim/.vimrc
ln -sf .vim/.gvimrc
cd $CWD
vim +PlugInstall
